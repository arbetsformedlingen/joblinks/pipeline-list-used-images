# List used images in JobAdLinks Pipeline

## Usage
The list of images is printed to stdout. Don't run podman with `-t` as it will mix stderr and stdout.

```
podman run --rm -i slask 2>/dev/null
```
