#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

WORKDIR=$(mktemp -d)

ENV=${1:-prod}


err_report() {
    echo "Error on line $1"
}

trap 'err_report $LINENO' ERR



COMMIT=$(curl -s https://gitlab.com/arbetsformedlingen/joblinks/pipeline-gitops-for-servers/-/raw/master/bin/run_"$ENV".sh | grep "^PIPELINECOMMIT=" | sed 's|^PIPELINECOMMIT=||')

cd "$WORKDIR"

git clone --recurse-submodules https://gitlab.com/arbetsformedlingen/joblinks/pipeline.git

cd pipeline

git checkout "$COMMIT"

git submodule update --recursive

git submodule --quiet foreach 'slug=$(basename $(git remote get-url origin) .git); echo "{ \"repo\": \"$(git remote get-url origin)\", \"image\": \"docker-images.jobtechdev.se/joblinks/$slug:$(git rev-parse --short HEAD)\" }"'
