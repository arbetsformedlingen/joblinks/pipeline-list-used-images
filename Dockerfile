FROM alpine:3.21.3

RUN apk update && apk add git curl bash

WORKDIR /tmp

ADD print_images.sh /tmp

ENTRYPOINT ["/tmp/print_images.sh"]
